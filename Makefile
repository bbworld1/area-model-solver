dep:
	pip3 install -r requirements.txt

install:
	chmod a+x area_model.py
	cp area_model.py /usr/local/bin/cpm-area-solver
	chown root:root /usr/local/bin/cpm-area-solver
