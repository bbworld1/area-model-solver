# Area Model Solver: factor area models!

Some tips:
1. DO NOT LOOK AT THE SOURCE CODE. It is ugly AF.
2. This program can currently only factor polynomials given a single root. If you give it anything else it probably won't work. I'm working on this but it'll probably take a bit.
3. Polynomials are written as (coeff)(var)^(power). Example: 3x^3


# Running/Installation
NOTE: The following section assumes you have Python3 and pip3 installed somewhere. If you don't, please install them before continuing.

To run it:
1. `pip3 install -r requirements.txt` (because i need nice ASCII table libraries)
2. `python3 area_model.py`

To install it:
1. If you didn't already run `pip3 install -r requirements.txt` run it now.
2. `sudo make install`
3. You can now run it with `cpm-area-solver`



Roadmap stuff:
1. Plug this thing into an OCR and have it fill out your homework for you
2. Handling multiple roots better
3. Bugfixes and crap

Pull/merge requests are always welcome!

